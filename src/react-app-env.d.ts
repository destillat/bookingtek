/// <reference types="react-scripts" />

declare namespace NodeJS {
    interface ProcessEnv {
        readonly REACT_APP_SPOONACULAR_API: string;
        readonly REACT_APP_SPOONACULAR_API_KEY: string;
    }
}