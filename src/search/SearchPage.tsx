import { Button, styled, TextField } from '@material-ui/core';
import { RecentSearches } from 'components/RecentSearches';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useRecentSearches } from './RecentSearchesProvider';
import { makeSearchResultPath } from './SearchPaths';

const PageContainer = styled('form')({
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '2rem',
});

const SearchBar = styled('div')({
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
});

const RecentSearchesContainer = styled('div')({
    flex: 1,
});

interface SearchFormValues {
    search: string;
}

export const SearchPage = () => {
    const { control, handleSubmit } = useForm<SearchFormValues>();
    const history = useHistory();
    const { searches, addSearch } = useRecentSearches();

    const onSubmit: SubmitHandler<SearchFormValues> = (data) => {
        const newUrl = makeSearchResultPath(data.search);

        addSearch(data.search);

        history.push(newUrl);
    };

    return (
        <PageContainer onSubmit={handleSubmit(onSubmit)}>
            <SearchBar>
                <Controller
                    name="search"
                    control={control}
                    defaultValue={''}
                    rules={{ required: true }}
                    render={({ field }) => (
                        <TextField
                            label="Ingredient Query"
                            variant="outlined"
                            {...field}
                        />
                    )}
                />
            </SearchBar>

            <RecentSearchesContainer>
                <RecentSearches searches={searches} />
            </RecentSearchesContainer>

            <Button type="submit" color="primary" variant="contained">
                Search
            </Button>
        </PageContainer>
    );
};
