export const SearchPaths = {
    search: '/search',
    searchResult: '/search/result',
};

export const makeSearchResultPath = (query: string) => {
    const searchParams = new URLSearchParams();
    searchParams.append('query', query);

    return `${SearchPaths.searchResult}?${searchParams.toString()}`;
};
