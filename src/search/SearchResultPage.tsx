import {
    Backdrop,
    CircularProgress,
    IconButton,
    styled,
    Typography,
} from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import { Alert } from '@material-ui/lab';
import { useRecipesSearchQuery } from 'api/queries/RecipesSearchQuery';
import { RecipesList } from 'components/RecipeList';
import { Link, useLocation } from 'react-router-dom';
import { SearchPaths } from './SearchPaths';

const AppBar = styled('div')({
    padding: '0 0.5rem',
    display: 'flex',
    alignItems: 'center',
});

const PageContainer = styled('div')({
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
});

export const SearchResultPage = () => {
    const { search } = useLocation();
    const urlParams = new URLSearchParams(search);
    const query = urlParams.get('query') ?? '';

    const { data, isLoading, isError } = useRecipesSearchQuery(query);

    const renderContent = () => {
        if (isError) {
            return (
                <Alert severity="error">
                    Some error ocured, please try again later
                </Alert>
            );
        }

        if (isLoading || !data) {
            return (
                <Backdrop open>
                    <CircularProgress color="inherit" />
                </Backdrop>
            );
        }

        if (data.totalResults === 0) {
            return (
                <Alert severity="info">
                    Your search - {query} - did not match any recipes.
                </Alert>
            );
        }

        return <RecipesList recipes={data.results} />;
    };

    return (
        <>
            <AppBar>
                <IconButton
                    component={Link}
                    to={SearchPaths.search}
                    edge="start"
                    color="inherit"
                    aria-label="Go back"
                >
                    <ArrowBack />
                </IconButton>
                <Typography variant="h6" noWrap>
                    Search result for "{query}"
                </Typography>
            </AppBar>
            <PageContainer>{renderContent()}</PageContainer>
        </>
    );
};
