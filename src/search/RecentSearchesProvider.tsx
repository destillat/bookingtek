import { createContext, ReactNode, useCallback, useContext, useMemo, useState } from "react";

interface RecentSearchesContextValue {
    searches: string[],
    addSearch: (search: string) => void;
}

const RecentSearchesContext = createContext<RecentSearchesContextValue>({
    searches: [],
    addSearch: () => {
        throw new Error('Not implemented');
    },
});

interface RecentSearchesProviderProps {
    children?: ReactNode;
}

export const RecentSearchesProvider = ({
    children,
}: RecentSearchesProviderProps) => {
    const [searches, setSearches] = useState<string[]>([]);

    const addSearch = useCallback((search: string) => {
        setSearches(searches => ([search, ...searches]));
    }, [])

    const contextValue = useMemo(() => ({
        searches,
        addSearch,
    }), [addSearch, searches])

    return <RecentSearchesContext.Provider value={contextValue}>
        {children}
    </RecentSearchesContext.Provider>
}

export const useRecentSearches = () => {
    return useContext(RecentSearchesContext);
}