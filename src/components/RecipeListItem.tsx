import {
    ListItem,
    ListItemAvatar,
    Avatar,
    ListItemText,
    Collapse,
    styled,
} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { RecipeShortInfo } from 'api/types/SearchRecipes';
import { useState } from 'react';

export interface RecipeListItemProps {
    recipe: RecipeShortInfo;
}

const Picture = styled('img')({
    width: '100%',
});

export const RecipeListItem = ({ recipe }: RecipeListItemProps) => {
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    return (
        <>
            <ListItem button onClick={handleClick}>
                <ListItemAvatar>
                    <Avatar src={recipe.image} alt={recipe.title}></Avatar>
                </ListItemAvatar>
                <ListItemText primary={recipe.title} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <Picture src={recipe.image} />
            </Collapse>
        </>
    );
};
