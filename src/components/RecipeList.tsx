import { List } from '@material-ui/core';
import { RecipeShortInfo } from 'api/types/SearchRecipes';
import { RecipeListItem } from './RecipeListItem';

export interface RecipesListProps {
    recipes: RecipeShortInfo[];
}

export const RecipesList = ({ recipes }: RecipesListProps) => {
    return (
        <List>
            {recipes.map((recipe) => (
                <RecipeListItem key={recipe.id} recipe={recipe} />
            ))}
        </List>
    );
};
