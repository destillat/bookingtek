import { List, ListItem, ListItemText, ListSubheader } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { makeSearchResultPath } from 'search/SearchPaths';

export interface RecentSearchesProps {
    searches: string[];
    amount?: number;
}

export const RecentSearches = ({
    searches,
    amount = 5,
}: RecentSearchesProps) => {
    return (
        <List
            subheader={
                <ListSubheader component="div">Recent Searches</ListSubheader>
            }
        >
            {searches.slice(0, amount).map((search) => (
                <ListItem component={Link} to={makeSearchResultPath(search)}>
                    <ListItemText primary={search} />
                </ListItem>
            ))}
        </List>
    );
};
