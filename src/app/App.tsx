import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Switch,
} from 'react-router-dom';
import { SearchPaths } from 'search/SearchPaths';
import { SearchPage } from 'search/SearchPage';
import { SearchResultPage } from 'search/SearchResultPage';
import { ApiProvider } from 'api/ApiProvider';
import { CssBaseline } from '@material-ui/core';
import { RecentSearchesProvider } from 'search/RecentSearchesProvider';

function App() {
    return (
        <>
            <CssBaseline />
            <ApiProvider>
                <RecentSearchesProvider>
                    <Router>
                        <Switch>
                            <Route
                                exact
                                path={SearchPaths.search}
                                component={SearchPage}
                            />
                            <Route
                                exact
                                path={SearchPaths.searchResult}
                                component={SearchResultPage}
                            />
                            <Route path="*">
                                <Redirect to={SearchPaths.search} />
                            </Route>
                        </Switch>
                    </Router>
                </RecentSearchesProvider>
            </ApiProvider>
        </>
    );
}

export default App;
