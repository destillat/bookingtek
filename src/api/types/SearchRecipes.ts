export interface RecipesSearchResult {
    results:      RecipeShortInfo[];
    offset:       number;
    number:       number;
    totalResults: number;
}

export interface RecipeShortInfo {
    id:        number;
    title:     string;
    image:     string;
    imageType: ImageType;
}

export enum ImageType {
    Jpg = "jpg",
}
