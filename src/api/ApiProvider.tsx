import { ReactNode } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';

interface ApiProviderProps {
    children?: ReactNode;
}

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 60000,
            notifyOnChangePropsExclusions: ['isStale'],
        },
    },
});

export const ApiProvider = ({ children }: ApiProviderProps) => {
    return (
        <QueryClientProvider client={queryClient}>
            {children}
        </QueryClientProvider>
    );
};
