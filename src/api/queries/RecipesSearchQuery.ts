import { useQuery } from 'react-query';
import { RecipesSearchResult } from 'api/types/SearchRecipes';

export const useRecipesSearchQuery = (searchQuery: string) => {
    return useQuery<RecipesSearchResult>(
        [
            'recipes',
            {
                searchQuery,
            },
        ],
        async () => {
            const url = new URL(
                '/recipes/complexSearch',
                process.env.REACT_APP_SPOONACULAR_API,
            );
            url.searchParams.append('query', searchQuery);
            url.searchParams.append(
                'apiKey',
                process.env.REACT_APP_SPOONACULAR_API_KEY,
            );
            const response = await fetch(url.toString());
            if (!response.ok) {
                throw new Error('Some api error description')
            }
            return (await response.json()) as RecipesSearchResult;
        },
    );
};
